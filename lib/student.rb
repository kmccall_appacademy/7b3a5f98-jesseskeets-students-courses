class Student
  attr_reader :courses, :first_name, :last_name
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    first_name + " " + last_name
  end

  # def enroll(new_course)
  #   if has_conflict?(new_course)
  #     raise "course conflicts with already enrolled course"
  #   elsif !@courses.include?(new_course)
  #     @courses << new_course
  #     new_course.students << self
  #   end
  # end
  def enroll(new_course)


    if has_conflict?(new_course)
      raise "Course Conflicts"
    elsif  courses.include?(new_course) == false
        self.courses << new_course
      end

    new_course.students << self
  end

  def course_load
    load = Hash.new(0)
      self.courses.each do |course|
        load[course.department] += course.credits
      end
    load
  end

  def has_conflict?(new_course)
    @courses.each do |course|
      if course.conflicts_with?(new_course)
        return true
      end
    end
    false
  end

end
